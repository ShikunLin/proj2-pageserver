# Project2-Pageserver #
Author: Shikun Lin  
Contact address: shikunl@uoregon.edu

## Description ##
This is a simple flask web sever which can return the html files if the files are existed, and return error and error html file when the request file is not existed or the request is forbidden

## How to Run ##


1. Open the terminal, then cd to the root directory which contain Dockfile
2. Build the image with 
 ```
  docker build -t flask-demo .
  ```
  
3. Run the container using
  
  ```
  docker run -d -p 5000:5000 flask-demo
  ```
4. Launch  [http://127.0.0.1:5000](http://127.0.0.1:5000/)  using web browser
