'''
Author: Shikun Lin
CIS322 18F UO

This is a simple flask web sever which can
return the html files if the files are exsited
Rerun error and error html file when the request file is not
exsited or the request is forbidden

'''

from flask import Flask
from flask import render_template
from flask import request
app = Flask(__name__)

#make trivia.html as home page of the web server
@app.route("/")
def index():
    return render_template("trivia.html"), 200

#allow user use trivia.html to log the home page
@app.route("/trivia.html")
def trivia():
    return render_template("trivia.html"), 200

#This is error handler for flask server which will check the file is
#exsited or not
@app.errorhandler(404)
def page_not_found(error):
    res = request.path

    #holder for illegal symbols
    illegal_symbols = ["..","/~","//","/."," ","*/","/*","~/","./"]

    #check the request contians illegal symbol or not. If yes, return 403
    #if not return 404
    for symbol in illegal_symbols:
        if symbol in res:
            return render_template('403.html'), 403


    return render_template('404.html'), 404

#To make sure all the 403 cases are included in the error handler
@app.errorhandler(403)
def page_forbidden(error):
    return render_template('403.html'), 403


if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
